; Logic
(def parseCustFile
  (->> "cust.txt"
       slurp
       clojure.string/split-lines
       (map #(clojure.string/split % #"\|"))))
(def customer-list
  (mapv vec parseCustFile))

; For Product

(def parseProdFile
  (->> "prod.txt"
       slurp
       clojure.string/split-lines
       (map #(clojure.string/split % #"\|"))))
(def product-list
  (mapv vec parseProdFile))

; For Sales

(def parseSaleFile
  (->> "sales.txt"
       slurp
       clojure.string/split-lines
       (map #(clojure.string/split % #"\|"))))
(def sales-list
  (mapv vec parseSaleFile))

; No lof lines in file
(def customer-size (atom (count customer-list)))
(def product-size (atom (count product-list)))
(def sales-size (atom (count sales-list)))

; Option three workout
(defn optionThreeWorkOut []
  (loop [x 0]
    (when (< x @sales-size)
      (print (get (get sales-list x) 0) ":")
      (loop [y 0]
        (when (< y @customer-size)
          (if (= (get (get sales-list x) 1) (get (get customer-list y) 0))
            (print "[" (get (get customer-list y) 1)))
          (recur (+ y 1))))
      (loop [z 0]
        (when (< z @product-size)
          (if (= (get (get sales-list x) 2) (get (get product-list z) 0))
            (print "," (get (get product-list z) 1)))
          (recur (+ z 1))))
      (println "," (get (get sales-list x) 3) "]")
      (recur (+ x 1)))))

; Option four WorkOut

(defn optionFourWorkOut []
  (println "Enter username :")
  (def user-input (read-line))
  (def customer_id "")
  (def result 0.0)
  (loop [x 0]
    (when (< x @customer-size)
      (if (= (compare user-input (get-in customer-list [x 1])) 0)
        (do (def customer_id (get-in customer-list [x 0])) ""))
      (recur (+ x 1))))
  (loop [y 0]
    (when (< y @sales-size)
      (if (= (compare customer_id (get-in sales-list [y 1])) 0)
        (do (let [product_amount (get-in sales-list [y 3]) product_id (get-in sales-list [y 2])]
              (loop [z 0]
                (when (< z @product-size)
                  (if (= (compare product_id (get-in product-list [z 0])) 0)
                    (do (def result (* (+ (Float/parseFloat (get-in product-list [z 2])) (Float/parseFloat product_amount))))))
                  (recur (+ z 1)))))))
      (recur (+ y 1))))
  (println (format "Total sales for %s : %.2f" user-input result)))

; Option Five workout
(defn optionFiveWorkOut []
  (println "Enter product name :")
  (def user_input (read-line))
  (def temp_product "")
  (def temp_product_count 0)
  (loop [x 0]
    (when (< x @product-size)
      (if (= (compare user_input (get (get product-list x) 1)) 0)
        (do (def temp_product (get (get product-list x) 0)) ""))
      (recur (+ x 1))))
  (loop [y 0]
    (when (< y @sales-size)
      (if (= (compare temp_product (get (get sales-list y) 2)) 0)
        (do (def temp_product_count (+ temp_product_count (Integer/parseInt (get (get sales-list y) 3))))))

      (recur (+ y 1))))
  (println (format "No. of %s in list :  %s" user_input temp_product_count)))
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; Option One
(defn option-one []
  (doseq [line (line-seq (clojure.java.io/reader "cust.txt"))]
    (let [[id name address phone-number] (.split #"\|" line)]
      (println (format "%s:[%s, %s, %s]" id name address phone-number)))))

; Option Two
(defn option-two []
  (doseq [line (line-seq (clojure.java.io/reader "prod.txt"))]
    (let [[id eatable-item price] (.split #"\|" line)]
      (println (format "%s:[%s, %s]" id eatable-item price)))))

; Option Three
(defn option-three []
  (optionThreeWorkOut))

; Option Four
(defn option-four []
  (optionFourWorkOut))

; Option Five
(defn option-five []
  (optionFiveWorkOut))

; That's a main villian
(defn -main []
  ; Here we go loop
  (loop []
   ; Display menu to user to select options from
    (println "*** Sales Menu *** \n
        1. Display Customer Table
        2. Display Product Table
        3. Display Sales Table
        4. Total Sales for Customer
        5. Total Count for Product
        6. Exit \n
        Enter an option?
      ")
    ; Take user input
    (let [user_input (Integer/parseInt (read-line))]

      ; Condition to loop menu till user request for exit
      ; First Condition
      (cond (= user_input 1)
            (do
              (option-one)
              (recur))
            (= user_input 2)
            (do (option-two)
                (recur))
            (= user_input 3)
            (do (option-three)
                (recur))
            (= user_input 4)
            (do (option-four)
                (recur))
            (= user_input 5)
            (do (option-five)
                (recur))
            :else
            (println "Good Bye")))))

(-main)
